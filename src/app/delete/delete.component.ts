import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import { ContactService } from '../contact/contact.service';
import {MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteComponent>,private contactservice: ContactService,@Inject(MAT_DIALOG_DATA) public data: any) { 
    console.log(data);
  }

  ngOnInit() {
  }
  onDelete(edetail:any){
    this.contactservice.delete(edetail);
    console.log(edetail);
      }
      
}
