import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';

import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({                                /* lets Angular know that a class can be used with the dependency injector.*/
 providedIn: 'root'
})

export class LeaderService {

  constructor() { }

  getLeaders():Observable<Leader[]>{
    return of(LEADERS).pipe(delay(2000));   //A pipe takes in data as input and transforms it to a desired output.
 
}
  getLeader(id:number):Observable<Leader>{
    return of(LEADERS.filter((lead)=>(lead.id ===id))[0]).pipe(delay(2000));  // filter is used to Select a subset of items from an array.
 
}
  getFeaturedLeader():Observable<Leader>{
    return of(LEADERS.filter((lead)=>(lead.featured))[0]).pipe(delay(2000));
}
}
