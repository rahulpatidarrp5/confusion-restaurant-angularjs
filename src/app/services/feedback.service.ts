import { Injectable,Inject } from '@angular/core';
import {Feedback} from '../shared/feedback';
import { Observable } from 'rxjs';
import {Restangular} from 'ngx-restangular';
import { TOKEN } from '../shared/baseurl';
import { Employeedetail } from '../shared/feedback';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
   
  feedback :Feedback;
  edetail: Employeedetail;
  constructor(private restangular:Restangular,@Inject(TOKEN) private baseURL) { }

  submitFeedback(feedback:Feedback):Observable<Feedback> {

    return this.restangular.all('feedback').post(feedback);
    
    }
    submitEmployeeDetail( edetail: Employeedetail):Observable<Employeedetail> {

      return this.restangular.all('employeedetail').post(edetail);
}

}