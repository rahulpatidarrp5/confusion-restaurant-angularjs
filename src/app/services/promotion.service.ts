import { Injectable,Inject } from '@angular/core';
import { Promotion } from '../shared/promotion';
import {Restangular} from 'ngx-restangular';
import {  Observable } from 'rxjs';
import { TOKEN } from '../shared/baseurl';

@Injectable(
)

export class PromotionService {

  constructor(private restangular:Restangular,@Inject(TOKEN) private baseURL) { }

  getPromotions():Observable<Promotion[]>{
    return this.restangular.all('promotions').getList();

}
  getPromotion(id:number):Observable<Promotion>{
    return  this.restangular.one('promotions',id).get();
}
  getFeaturedPromotion():Observable<Promotion>{
    return this.restangular.all('promotions').getList({featured:true}).map(promotions => promotions[0]);
}
getPromotionIds():Observable<number[] | any>{           //map method is used to extract the JSON content from the response and use it then in the observable processing
  return this.getPromotions()
  .map(promotions => { return promotions.map(promotion => promotion.id)})
  .catch (error => {return error;});     
 }
}
