import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatCardModule, MatToolbarModule, MatListModule, MatGridListModule, MatMenuModule, MatButtonModule, MatCheckboxModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSliderModule } from '@angular/material/slider';
 
import 'hammerjs';
import {RestangularModule,Restangular} from 'ngx-restangular';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { MatDialogModule } from '@angular/material';
import { DishdetailComponent } from './dishdetail/dishdetail.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
 
import { DishService } from './services/dish.service';
import { PromotionService } from './services/promotion.service';
import { LeaderService } from './services/leader.service';
 
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginComponent } from './login/login.component'; 

import { ProcessHTTPMsgService } from './services/process-httpmsg.service';
import { TOKEN } from './shared/baseurl';
import {RestangularConfigFactory} from './shared/restConfig';
import { HighlightsDirective } from './directives/highlights.directive';
import { ContactService } from './contact/contact.service';
import { DeleteComponent } from './delete/delete.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DishdetailComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
    HighlightsDirective,
    DeleteComponent,
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatListModule,
    MatMenuModule,
    MatGridListModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule ,
    MatProgressSpinnerModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatInputModule,
    MatSliderModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    HttpModule
  ],
  providers: [DishService,PromotionService,LeaderService,ContactService,
    {provide:TOKEN,useValue:'http://localhost:3000/'},
  ProcessHTTPMsgService ], // BaseURL as a value provider
  entryComponents:[LoginComponent,DeleteComponent,ContactComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
