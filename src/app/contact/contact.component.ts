import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { flyInOut } from '../animations/app.animation';
import { FeedbackService } from '../services/feedback.service';
import {visibility,expand} from '../animations/app.animation';
import { MatDialog,MatDialogRef} from '@angular/material';
import { Employeedetail } from '../shared/feedback';
import { ContactService } from './contact.service';
import { DeleteComponent } from '../delete/delete.component';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display:block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

  @ViewChild('fform') feedbackFormDirective;      //ViewChild is used to get the first element or the directive matching the selector from the view DOM. If the view DOM changes, and a new child matches the selector, the property will be updated.
 
  @ViewChild('fform')edetailFormDirective;

  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType = ContactType;
  feedbackcopy = null;
  submitted = false;
  feedback2 = null;
  feedbackErrMess: string;
  edetail: Employeedetail;
  edetailForm:FormGroup;
   employee;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': '',
  };
  validationMessages = {
    'firstname': {
      'required': 'First Name is required.',
      'minlength': 'First Name must be atleast 2 characters long',
      'maxlength': 'First Name can not be more than 25 characters long',
    },
    'lastname': {
      'required': 'Last Name is required.',
      'minlength': 'Last Name must be atleast 2 characters long',
      'maxlength': 'Last Name can not be more than 25 characters long',
    },
    'telnum': {
      'required': 'Tel. Number is required',
      'pattern': 'Tel. Number must contain only numbers',
    },
    'email': {
      'required': 'Email is required',
      'email': 'Email not in valid format',
    },
    
  };

  constructor(private fb: FormBuilder, private feedbackservice: FeedbackService,public dialog:MatDialog,
    private ed: FormBuilder,private contactservice: ContactService,) {
    this.createForm();
    this.createform();
  }

  ngOnInit() {
    
  }


  createForm() {
    this.feedbackForm = this.fb.group({                    // construct a formgroup with a given map of configuration
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: ['', [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();    // reset form validation messages
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '';
        }
      }
    }

  }
  onSubmit() {
    this.submitted = true;
    this.feedback = this.feedbackForm.value;
    // this.feedbackcopy.save().subscribe(feedback => this.feedback=feedback);
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: '',
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackFormDirective.resetForm();
    this.feedbackservice.submitFeedback(this.feedback)
      .subscribe(feedback => {
        this.feedback2 = feedback;
        this.submitted = false;
        setTimeout(() => {
          this.feedback2 = null;
        }, 5000);
      },
        errmess => { this.feedback2 = null; this.feedbackErrMess = <any>errmess; });
    console.log(this.feedback);
  }

                           /* for Employee details*/
 formerrors = {      
   'ename':''
    };          
    validationmessages = {
      'ename': {
        'required': 'Employee Name is required.',
        'minlength': 'Employee Name must be atleast 2 characters long',
        'maxlength': 'EmployeeName can not be more than 25 characters long',
      },      
    };

    createform() {
      this.edetailForm = this.ed.group({                    // construct a formgroup with a given map of configuration
        ename: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
        eid:'',
        company:'',
        dob:''
      });
  
      this.edetailForm.valueChanges
        .subscribe(data => this.onValuechanged(data));
      this.onValuechanged();    // reset form validation messages
    }

    onValuechanged(data?: any) {
      if (!this.edetailForm) { return; }
      const form = this.edetailForm;
  
      for (const field in this.formerrors) {
        this.formerrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationmessages[field];
          for (const key in control.errors) {
            this.formerrors[field] += messages[key] + '';
          }
        }
      }
  
    }

  onAdd(){
    this.edetail = this.edetailForm.value;
    console.log(this.edetail);
    this.edetailForm.reset({
      eid:'',
      ename:'',
      company:'',
      dob:''
    });
    this.edetailFormDirective.resetForm();
    this.contactservice.EmployeeDetail(this.edetail); 
    this.employee = this.contactservice.getEmployeeDetail();
    console.log(this.employee); 
    
    // this.feedbackservice.submitEmployeeDetail(this.edetail)
    //   .subscribe(edetail =>this.edetail=edetail);
  }
  
  onSave(){
    this.edetail = this.edetailForm.value;
    this.edetailForm.reset({
      eid:'',
      ename:'',
      company:'',
      dob:''
    });
    this.edetailFormDirective.resetForm();
    this.contactservice.EmployeeDetail(this.edetail); 
    this.employee = this.contactservice.getEmployeeDetail();

    this.feedbackservice.submitEmployeeDetail(this.edetail)
      .subscribe(edetail =>this.edetail=edetail);
  }

  onEdit(edetail:any){
     console.log(edetail);
     this.edetailForm.reset({
      eid:edetail.eid,
      ename:edetail.ename,
      company:edetail.company,
      dob:edetail.dob
    });
    this.edetailFormDirective.resetForm();
  }

  

openLoginForm() : void {
   this.dialog.open(DeleteComponent,{width: '350px', height: '200px',data:{id:'edetail'}});
}

}
