import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

   employee=[];
  constructor() { }

   EmployeeDetail(detail){
    this.employee.push(detail);
    console.log(this.employee);
  }
   getEmployeeDetail(){
    return this.employee;
  }
  delete(id) {

        let index = this.employee.findIndex(edetail => edetail._id === id);
          this.employee.splice(index, 1);
     }
}
