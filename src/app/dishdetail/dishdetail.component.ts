import { Component, OnInit ,Inject,ViewChild} from '@angular/core';
import {Params,ActivatedRoute} from '@angular/router'; // Contains the information about a route associated with a component loaded in an outlet
import { Location} from '@angular/common'; //A service that applications can use to interact with a browser's URL.

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import 'rxjs/add/operator/switchMap';                             //switchMap could be used — when a new search is made, pending results are no longer needed; and
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { Comment } from '../shared/comment';
import { TOKEN } from '../shared/baseurl';
import {visibility,flyInOut,expand} from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host:{
 '[@flyInOut]':'true',
 'style':'display:block;'
  },
  animations:[
    flyInOut(), 
    visibility(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {
  @ViewChild('fform') AuthorfeedbackFormDirective; 

  dish : Dish;
  dishcopy=null;
  dishIds:number[];
  prev:number;
  next:number;
  
  AuthorfeedbackForm:FormGroup;
  comments:Comment;
  errMess:string;
  visibility='shown';

  formErrors = {
    'author':'',

};
  validationMessages = {
  'author' : {
    'required':' Name is required.',
    'minlength': ' Name must be atleast 2 characters long',
    'maxlength':' Name can not be more than 25 characters long',
  }
};

  
  constructor(private dishservice:DishService,
    private route:ActivatedRoute,
    private location:Location,
     private fb:FormBuilder,
     @Inject(TOKEN) private BaseURL) { 
       
     }

  ngOnInit() {
    this.createForm();

     this.dishservice.getDishIds().subscribe (dishIds => this.dishIds = dishIds);

    this.route.params.switchMap((params:Params)=>{ this.visibility='hidden';
      return  this.dishservice.getDish(+params['id']);})
    .subscribe(dish=> {this.dish = dish;this.dishcopy=dish; this.setPrevNext(dish.id);this.visibility='shown';},
     errmess =>this.errMess = <any>this.errMess);
    
    }

    setPrevNext(dishId:number){
   let index=this.dishIds.indexOf(dishId);
   this.prev=this.dishIds[(this.dishIds.length + index - 1 )% this.dishIds.length];
   this.next=this.dishIds[(this.dishIds.length + index + 1 )% this.dishIds.length];
    }

    goback():void{
     this.location.back();
    }

    createForm(){
      this.AuthorfeedbackForm=this.fb.group({

        author:['',[Validators.required,Validators.minLength(2),Validators.maxLength(25)]],
        rating:'',
        comment:''
      });

      this.AuthorfeedbackForm.valueChanges
      .subscribe(data =>this.onValueChanged(data));
      this.onValueChanged();    // reset form validation messages
       }
       onValueChanged(data?:any){
        if(!this.AuthorfeedbackForm) { return ;}
        const form = this.AuthorfeedbackForm;

        for (const field in this.formErrors){
          this.formErrors[field]='';
          const control = form.get(field);
          if(control && control.dirty && !control.valid){
            const messages = this.validationMessages[field];
            for (const key in control.errors){
              this.formErrors[field] += messages [key] + '';
          }
        }
      }
      

    }

   

    onSubmit() {
      this.comments = this.AuthorfeedbackForm.value; 
      this.comments.date =new Date().toISOString(); //The toISOString() method converts a Date object into a string,
      console.log(this.comments);
      this.dishcopy.comments.push(this.comments);
      this.dishcopy.save().subscribe(dish => this.dish=dish);
      this.AuthorfeedbackForm.reset({
        author:'',
        rating:'',
        comment:''
});
this.AuthorfeedbackFormDirective.resetForm();
}
}